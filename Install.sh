#!/bin/bash

# Copyright 2018, "fffred" <fredpz@zoho.eu>

# Primitivistical is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Primitivistical is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Primitivistical.  If not, see <https://www.gnu.org/licenses/>.

THEME_NAME=Primitivistical

# Verify a bunch of parameters
if [[ $EUID -ne 0 ]]; then
    echo "Root privileges needed" 1>&2
    exit 1
fi

GRUB_DIR=/boot/grub2
if [[ ! -d $GRUB_DIR ]]; then
    GRUB_DIR=/boot/grub
    if [[ ! -d $GRUB_DIR ]]; then
        echo "Could not find the grub directory" 1>&2
        exit 1
    fi
fi

GRUB_MKCONFIG=grub2-mkconfig
if ! command -v $GRUB_MKCONFIG >/dev/null 2>&1 ; then
    GRUB_MKCONFIG=grub-mkconfig
    if ! command -v $GRUB_MKCONFIG >/dev/null 2>&1 ; then
        echo "Command '$GRUB_MKCONFIG' not found" 1>&2
        exit 1
    fi
fi

# Find out the screen dpi
SCALING=1
if ! command -v hwinfo >/dev/null 2>&1 ; then
    echo "Command 'hwinfo' not found. Cannot determine monitor dpi"
else
    MONITOR=`hwinfo --monitor`
    if [ -n "$MONITOR" ]; then
        MODEL=$(echo "$MONITOR" | grep -m 1 Model)
        if [ -n "$MODEL" ]; then
            echo "Found monitor"$MODEL
        fi
        SIZE=$(echo "$MONITOR" | grep -m 1 Size | sed -n -E 's/ *Size[ :]*([0-9]+)x[0-9]+.*/\1/p')
        if [ -n "$SIZE" ]; then
            RES=$(echo "$MONITOR" | grep -m 1 Resolution | sed -n -E 's/ *Resolution[ :]*([0-9]+)x[0-9]+.*/\1/p')
            SCALING=$(($RES / $SIZE / 3))
            SCALING=$(($SCALING<1?1:$SCALING))
            SCALING=$(($SCALING>4?4:$SCALING))
        fi
    fi
fi

# Change sizes according to dpi
FONT_SIZE=$((13 * $SCALING)) 
ICON_WIDTH=$((25 * $SCALING))
ICON_HEIGHT=$((25 * $SCALING))
ITEM_ICON_SPACE=$((7 * $SCALING))
ITEM_HEIGHT=$((30 * $SCALING))
ITEM_SPACING=$((5 * $SCALING))

# Copy folder to themes
mkdir -p $GRUB_DIR/themes
THEME_DIR=$GRUB_DIR/themes/$THEME_NAME
rm -rf $THEME_DIR
cp -rf $THEME_NAME $GRUB_DIR/themes
cp "Fonts/DejaVuSans"$FONT_SIZE".pf2" $THEME_DIR
sed -i 's/ICON_WIDTH/'"$ICON_WIDTH"'/' $THEME_DIR/theme.txt
sed -i 's/ICON_HEIGHT/'"$ICON_HEIGHT"'/' $THEME_DIR/theme.txt
sed -i 's/ITEM_ICON_SPACE/'"$ITEM_ICON_SPACE"'/' $THEME_DIR/theme.txt
sed -i 's/ITEM_HEIGHT/'"$ITEM_HEIGHT"'/' $THEME_DIR/theme.txt
sed -i 's/ITEM_SPACING/'"$ITEM_SPACING"'/' $THEME_DIR/theme.txt

# Replace GRUB_THEME variable
sed -i '/GRUB_THEME=/d' /etc/default/grub
sed -i -e '$a\' /etc/default/grub
echo "GRUB_THEME=\""$THEME_DIR"/theme.txt\"" >> /etc/default/grub

# Update grub config
$GRUB_MKCONFIG -o $GRUB_DIR/grub.cfg

echo -e $THEME_NAME" installed, please reboot"



